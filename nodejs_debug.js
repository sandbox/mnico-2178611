/**
 * @file
 * 
 */
(function ($) {

Drupal.Nodejs.callbacks.nodejsDebug = {
  callback: function (message) {
    Drupal.nodejs_ajax.runCommands(message);
    
    var lastDebug = $('.nodejs-debug-need-process.nodejs-debug-item');
    lastDebug.children('.kint')
      .children('dl')
      .children('dt')
      .children('dfn')
      .text(lastDebug.attr('rel'));
    lastDebug.removeClass('nodejs-debug-need-process');
    // No need the footer info.
    $('.kint footer').remove();
    // Remove the init tips.
    $('.nodejs-debug-item.nodejs-debug-tips').remove();
  }
};


Drupal.behaviors.nodejsDebug = {
  attach: function(context, settings) {
    $('.nodejs-debug-clean', context).click(function() {
      $('#nodejs-debug').empty()
        .append(settings.nodejsDebug.initMessage);
    });
  }
}

}(jQuery));