<?php
/**
 * @file
 */


/**
 * Implements hook_permission().
 */
function nodejs_debug_permission() {
  return array(
    'access nodejs debug' => array(
      'title' => t('Access to nodejs debug system.')
    ),
    'run nodejs debug system' => array(
      'title' => t('Run nodejs debug'),
      'description' => t('If the user has this permission, all debug found was run'),
    ), 
  );
}
 
 
/**
 * Implements hook_menu().
 */
function nodejs_debug_menu() {
  $items['nodejs-debug'] = array(
    'title' => 'Debug with Nodejs',
    'page callback' => 'nodejs_debug_page_debug',
    'access arguments' => array('access nodejs debug'),
    'file' => 'nodejs_debug.pages.inc',
  );
  $items['nodejs-debug/test'] = array(
    'title' => 'Test Nodejs Debug',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nodejs_debug_page_test_form'),
    'access arguments' => array('access nodejs debug'),
    'file' => 'nodejs_debug.pages.inc',
  );

  return $items;
}


/**
 * If the function nd() not exists will be the alias of nodejs_debug_dd().
 */
if (!function_exists('nd')) {
  function nd($data, $options = array()) {
    nodejs_debug_dd($data, $options);
  }
}


/**
 * This function debug the variable with NodeJS.
 * 
 * @param mixed $data
 *   The variable that will be debugged.
 * @param mixed $options
 *   Can be an array with the keys:
 *     - label: This is the label that appears on top of the variable.
 *     - class: You can add a class to the element to be detected more easily.
 *   If you set a string will be the label. 
 */
function nodejs_debug_dd($data, $options = array()) {
  if (!nodejs_debug_load_kint() || !user_access('run nodejs debug system')) {

    return;
  }
  global $user;
  
  $label = $class = '';
  if (is_array($options)) {
    $label = isset($options['label']) ? $options['label'] : '';
    $class = isset($options['class']) ? $options['class'] : '';
  }
  elseif (is_string($options)) {
    $label = $options;
  }
  
  
  $bt = debug_backtrace();
  $dump = @Kint::dump($data);
  $function_caller = '';
  $final_callee = array();
  
  while ( $callee = array_pop( $bt ) ) {
    if ( strtolower( $callee['function'] ) === 'nodejs_debug_dd' || strtolower( $callee['function'] ) === 'nd') {
      break;
    } else {
      $function_callee = $callee['function'];
    }
  }
  
  $parameter = _nodejs_debug_get_parameter_name($callee);

  $file = str_replace($_SERVER['DOCUMENT_ROOT'], 'ROOT', $callee['file']);
  
  
  $items = array(
    $file . ' line ' . $callee['line'],
    'function ' . $function_callee,
    'time: ' . format_date(time(), 'custom', 'H:i:s'),
    'User:' . $user->name,
  );
  $data = theme('item_list', array('items' => $items));
  $item = "<div class='nodejs-debug-need-process nodejs-debug-item $class' rel='$parameter'>$label $dump <div class='nodejs-debug-item-data'>$data</div></div>";

  $commands[] = ajax_command_prepend('#nodejs-debug', $item);
  $message = (object) array(
    'channel' => 'nodejs_debug_channel',
    'commands' => $commands,
    'callback' => 'nodejsDebug',
  );

  nodejs_send_content_channel_message($message);
}


/**
 * Ajax callback.
 */
function nodejs_debug_page_test_form_ajax($form, &$form_state) {

  return $form['choose'];
}


/**
 * Implements hook_libraries_info().
 */
function nodejs_debug_libraries_info() {
  return array(
    'kint' => array(
      'name' => 'Kint',
      'vendor url' => 'http://raveren.github.io/kint/',
      'download url' => 'https://github.com/raveren/kint/releases/tag/v0.9',
      'version' => 0.9,
      'files' => array(
        'php' => array(
          'Kint.class.php',
        ),
      ),
    ),
  );
}


/**
 * Load Kint library.
 * 
 * @return boolean
 *   TRUE if the library is loaded.
 */
function nodejs_debug_load_kint() {
  $library = libraries_load('kint');
  if ($library && !empty($library['loaded'])) {
    
    return TRUE;
  }
  else {
    $message = t('The kint library was not found! Please following the instructions from the README.txt to get the library.');
    drupal_set_message(check_plain($message), $type = 'error');
  }

  return FALSE;
}


/**
 * This function get the parameter name.
 */
function _nodejs_debug_get_parameter_name($callee) {
  $source = _nodejs_debug_get_source_file($callee['file'], $callee['line']); 

  $code_pattern = empty( $callee['class'] )
  ? $callee['function']
  : $callee['class'] . "\x07*" . $callee['type'] . "\x07*" . $callee['function'];
  
  return _nodejs_debug_get_parameter_from_source($source, $code_pattern);
}

/**
 * This function get the content from file.
 */
function _nodejs_debug_get_source_file($file_calling, $limit) {
  $file   = fopen($file_calling, 'r');
  $line   = 0;
  $source = '';
  while ( ( $row = fgets( $file ) ) !== false) {
    if ( ++$line > $limit ) break;
    $source .= $row;
  }
  fclose( $file );

  return _nodejs_debug_remove_all_but_code( $source );
}

/**
 * Helper
 * 
 * Copy from method _removeAllButCode
 */
function _nodejs_debug_remove_all_but_code($source) {
  $newStr        = '';
  $tokens        = token_get_all( $source );
  $commentTokens = array( T_COMMENT => true, T_INLINE_HTML => true, T_DOC_COMMENT => true );

  defined( 'T_NS_SEPARATOR' ) or define( 'T_NS_SEPARATOR', 380 );

  $whiteSpaceTokens = array(
    T_WHITESPACE => true, T_CLOSE_TAG => true,
    T_OPEN_TAG   => true, T_OPEN_TAG_WITH_ECHO => true,
  );

  foreach ( $tokens as $token ) {
    if ( is_array( $token ) ) {
      if ( isset( $commentTokens[$token[0]] ) ) continue;

      if ( $token[0] === T_NEW ) {
        $token = 'new ';
      } elseif ( $token[0] === T_NS_SEPARATOR ) {
        $token = "\\\x07";
      } elseif ( isset( $whiteSpaceTokens[$token[0]] ) ) {
        $token = "\x07";
      } else {
        $token = $token[1];
      }
    } elseif ( $token === ';' ) {
      $token = "\x07";
    }

    $newStr .= $token;
  }
  return $newStr;
}


/**
 * Copy from method _getPassedNames.
 * 
 * @param string $source
 *   The source result.
 * @param string $codePattern
 *   The pattern of the function or class.
 * 
 * @return string
 *   The parameter name.
 */
function _nodejs_debug_get_parameter_from_source($source, $codePattern) {
  preg_match_all( "#[\x07{(](\\+|-|!|@)?{$codePattern}\x07*(\\()#i", $source, $matches, PREG_OFFSET_CAPTURE );
  $match    = end( $matches[2] );
  $modifier = end( $matches[1] );
  $modifier = $modifier[0];
  $passedParameters = str_replace( "\x07", '', substr( $source, $match[1] + 1 ) );
  $c          = strlen( $passedParameters );
  $inString   = $escaped = false;
  $i          = 0;
  $inBrackets = 0;
  while ( $i < $c ) {
    $letter = $passedParameters[$i];
    if ( $inString === false ) {
      if ( $letter === '\'' || $letter === '"' ) {
        $inString = $letter;
      } elseif ( $letter === '(' ) {
        $inBrackets++;
      } elseif ( $letter === ')' ) {
        $inBrackets--;
        if ( $inBrackets === -1 ) { # this means we are out of the brackets that denote passed parameters
          $passedParameters = substr( $passedParameters, 0, $i );
          break;
        }
      }
    } elseif ( $letter === $inString && !$escaped ) {
      $inString = false;
    }

    # place an untype-able character instead of whatever was inside quotes or brackets, we don't
    # need that info. We'll later replace it with '...'
    if ( $inBrackets > 0 ) {
      if ( $inBrackets > 1 || $letter !== '(' ) {
        $passedParameters[$i] = "\x07";
      }
    }
    if ( $inString !== false ) {
      if ( $letter !== $inString || $escaped ) {
        $passedParameters[$i] = "\x07";
      }
    }

    $escaped = !$escaped && ( $letter === '\\' );
    $i++;
  }

  # by now we have an unnested arguments list, lets make it to an array for processing further
  $arguments = explode( ',', preg_replace( "#\x07+#", '...', $passedParameters ) );

  # test each argument whether it was passed literary or was it an expression or a variable name
  $parameters = array();
  $blacklist  = array( 'null', 'true', 'false', 'array(...)', 'array()', '"..."', 'b"..."', );
  foreach ( $arguments as $argument ) {

    if ( is_numeric( $argument )
      || in_array( str_replace( "'", '"', strtolower( $argument ) ), $blacklist, true )
    ) {
      $parameters[] = null;
    } else {
      $parameters[] = trim( $argument );
    }
  }


  return array_shift($parameters);
}
