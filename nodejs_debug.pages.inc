<?php
/**
 * @file
 */

/**
 * 
 */
function nodejs_debug_page_debug() {
  if (!nodejs_send_content_channel_token('nodejs_debug_channel')) {
    drupal_set_message(t('Something is wrong with nodejs server.'), 'error');
  }

  $message = t('Here you can see any variable in real time with the Kint Library, use nodejs_debug_dd($my_variable) in your code. Then execute it and see how the information appear here');
  $init = '<div class="nodejs-debug-item nodejs-debug-tips">' . $message . '</div>';

  $output = '<a class="button nodejs-debug-clean">' . t('Clean results') . '</a>';

  $output .= '<div id="nodejs-debug">' . $init . '</div>';

  $path = drupal_get_path('module', 'nodejs_debug');
  drupal_add_js($path . '/nodejs_debug.js');
  drupal_add_css($path . '/nodejs_debug.css');
  drupal_add_js(array('nodejsDebug' => array('initMessage' => $init)), 'setting');

  return $output;
}

/**
 * 
 */
function nodejs_debug_page_test_form($form, &$form_state) {
  // We need to see what happend with the $_SERVER.
  nd($_SERVER["REQUEST_URI"], array(
    'label' => t('The $_SERVER["REQUEST_URI"] variable'),
    'class' => 'nodejs-debug-item-yellow',
    
  ));

  $markup = t('This page is only to test the debug system. This page call 3 times the nodejs_debug_dd() function. Change the select.');

  $form['message'] = array(
    '#markup' => $markup,
  );

  $form['select-ajax'] = array(
    '#type' => 'select',
    '#title' => t('Select anything'),
    '#options' => array(
      1 => t('One'),
      2 => t('Two'),
      3 => t('Three'),
    ),
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'nodejs_debug_page_test_form_ajax',
      'wrapper' => 'nodejs-debug-test-choose',
    ),
  );

  $choose = t('No value was selected');
  if (isset($form_state['values']['select-ajax'])) {
    nd($form_state, t('We need to know what happen with the form_state'));
    
    $choose = t('You select !value. See the nodejs debug page.', array('!value' => $form_state['values']['select-ajax']));
  }

  $form['choose'] = array(
    '#markup' => $choose,
    '#prefix' => '<div id="nodejs-debug-test-choose">',
    '#suffix' => '</div>',
  );
  return $form;
}
